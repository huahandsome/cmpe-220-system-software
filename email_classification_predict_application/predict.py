import import_data
import display_label
import numpy as np
import tensorflow as tf
import psutil

#########################
### GLOBAL PARAMETERS ###
#########################

# Get our dimensions for our different variables and placeholders:
# numFeatures = the number of words extracted from each email
numFeatures = 2955
# numLabels = number of classes we are predicting (here just 2: ham or spam)
numLabels = 2

#create a tensorflow session
sess = tf.Session()

####################
### PLACEHOLDERS ###
####################

# X = X-matrix / feature-matrix / data-matrix... It's a tensor to hold our email
# data. 'None' here means that we can hold any number of emails
X = tf.placeholder(tf.float32, [None, numFeatures])

# yGold = Y-matrix / label-matrix / labels... This will be our correct answers
# matrix. Every row has either [1,0] for SPAM or [0,1] for HAM. 'None' here
# means that we can hold any number of emails
yGold = tf.placeholder(tf.float32, [None, numLabels])

#################
### VARIABLES ###
#################

#all values must be initialized to a value before loading can occur

weights = tf.Variable(tf.zeros([numFeatures,numLabels]))

bias = tf.Variable(tf.zeros([1,numLabels]))

#predict visualization
tf_mem_usage = tf.placeholder("float", None)
tf_mem_usage_summary = tf.summary.scalar("mem_usage", tf_mem_usage)

tf_cpu_usage = tf.placeholder("float", None)
tf_cpu_usage_summary = tf.summary.scalar("c_usage", tf_cpu_usage)

########################
### OPS / OPERATIONS ###
########################

#since we don't have to train the model, the only Ops are the prediction operations

apply_weights_OP = tf.matmul(X, weights, name="apply_weights")
add_bias_OP = tf.add(apply_weights_OP, bias, name="add_bias")
activation_OP = tf.nn.sigmoid(add_bias_OP, name="activation")

#Initializes everything we've defined made above, but doesn't run anything
#until sess.run()
init_OP = tf.initialize_all_variables()

sess.run(init_OP)       #initialize variables BEFORE loading

#load variables from file
saver = tf.train.Saver()
saver.restore(sess, "trained_variables.ckpt")

predict_writer = tf.summary.FileWriter('./prediction_and_accuracy', sess.graph)

########################
###      METHODS     ###
########################

def prediction(features, epoch):
    #debug_reshape = features.reshape(1, len(features))

    #run through graph
    tensor_prediction = sess.run(activation_OP, feed_dict={X: features.reshape(1, len(features))})      #had to make sure that each input in feed_dict was an array
    predict_ret = display_label.labelToString(tensor_prediction)

    #visualization
    ret = sess.run(tf_mem_usage_summary, feed_dict={tf_mem_usage: psutil.virtual_memory().used})
    predict_writer.add_summary(ret, epoch)

    ret = sess.run(tf_cpu_usage_summary, feed_dict={tf_cpu_usage: psutil.cpu_percent(interval=None)})
    predict_writer.add_summary(ret, epoch)

    return predict_ret

def prediction_and_accuracy(features, goldLabel):
    tensor_prediction, accuracy = sess.run([activation_OP, accuracy_OP], feed_dict={X: features.reshape(1, len(features)), yGold: goldLabel.reshape(1, len(goldLabel))})      #had to make sure that each input in feed_dict was an array


if __name__ == "__main__":
    testX, testY = import_data.import_test_data()
    for i in range(len(testX)):
        predict_ret = prediction(testX[i], i)
        print(predict_ret)
