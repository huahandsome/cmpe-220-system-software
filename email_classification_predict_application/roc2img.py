#!/usr/bin/env python3
# (C) 2017 OpenEye Scientific Software Inc. All rights reserved.
#
# TERMS FOR USE OF SAMPLE CODE The software below ("Sample Code") is
# provided to current licensees or subscribers of OpenEye products or
# SaaS offerings (each a "Customer").
# Customer is hereby permitted to use, copy, and modify the Sample Code,
# subject to these terms. OpenEye claims no rights to Customer's
# modifications. Modification of Sample Code is at Customer's sole and
# exclusive risk. Sample Code may require Customer to have a then
# current license or subscription to the applicable OpenEye offering.
# THE SAMPLE CODE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED.  OPENEYE DISCLAIMS ALL WARRANTIES, INCLUDING, BUT
# NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
# PARTICULAR PURPOSE AND NONINFRINGEMENT. In no event shall OpenEye be
# liable for any damages or liability in connection with the Sample Code
# or its use.

#############################################################################
# Plots ROC curve
#############################################################################

import matplotlib.pyplot as plt

"""
Ham    -> Positive
Spam   -> Negative
result -> (expect, predict)
"""
def GetRates(result, total_ham, total_spam):

    tpr = [0.0]  # true positive rate
    fpr = [0.0]  # false positive rate

    found_ham  = 0.0
    found_spam = 0.0

    for element in result:
        #expect ham, and predict ham
        if element[0] == "ham" and element[1] == "ham":
            found_ham += 1.0
        elif element[0] == "spam" and element[1] == "ham":
            found_spam += 1.0

        tpr.append(found_ham / float(total_ham))
        fpr.append(found_spam / float(total_spam))

    return tpr, fpr


def SetupROCCurvePlot(plt):

    plt.xlabel("FPR", fontsize=14)
    plt.ylabel("TPR", fontsize=14)
    plt.title("ROC Curve", fontsize=14)


def SaveROCCurvePlot(plt, randomline=True):

    if randomline:
        x = [0.0, 1.0]
        plt.plot(x, x, linestyle='dashed', color='red', linewidth=2, label='random')

    plt.xlim(0.0, 1.0)
    plt.ylim(0.0, 1.0)
    plt.legend(fontsize=10, loc='best')
    plt.tight_layout()
    plt.savefig('client_roc.eps', format='eps', dpi=600)


def AddROCCurve(plt, result, total_ham, total_spam, color):

    tpr, fpr = GetRates(result, total_ham, total_spam)

    plt.plot(fpr, tpr, color=color, linewidth=2)


def DepictROCCurve(result, total_ham, total_spam, color, randomline=True):

    plt.figure(figsize=(4, 4), dpi=80)

    SetupROCCurvePlot(plt)
    AddROCCurve(plt, result, total_ham, total_spam, color)
    SaveROCCurvePlot(plt, randomline)
