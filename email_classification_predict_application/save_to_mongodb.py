########################
###      METHODS     ###
########################
import pymongo
from pymongo import MongoClient
import sys

def get_db():
    try:
        client = MongoClient('localhost:27017')
        db = client.email_database
    except pymongo.errors.ConnectionFailure as e:
        print(("!!!! Could not connect to server: %s !!!! " % e))
        sys.exit(-1)
    except :
        print("!!!!Error happened in get_db !!!! ")
        sys.exit(-1)
    return db

def insert_email_to_db(db, email, predict_ret):
    try:
        db.email_tbl.insert({"e_mail": email}, {"prediction": predict_ret})
    except pymongo.errors.OperationFailure as e:
        print(("!!!!Insert error: %s !!!! " % e))
        sys.exit(-1)
    except :
        print("!!!!Error happened in database insert !!!! ")
        sys.exit(-1)

def retrieve_email_from_db(db):
    array = list(db.email_tbl.find())
    return array

if __name__ == "__main__":
   db = get_db()
#   insert_email_to_db(db, "hadaldjfladjfladsf")
   collections = retrieve_email_from_db(db)
   print(collections)
