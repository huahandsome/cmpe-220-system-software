import numpy as np
import matplotlib.pyplot as plt


xData =  np.linspace(0,2*np.pi,100,endpoint=True)
yData1 = np.sin(xData)



plt.rc('text', usetex=False)
plt.rc('font', family='serif', size=18)
plt.rc('lines', linewidth=2, antialiased=True)
plt.rc('legend',fontsize=14, frameon=False)


plt.figure(num=1, figsize=(8, 6))
plt.ylabel(r'sin$(\theta)$')
plt.xlabel(r'$\theta$')


plt.plot(xData, yData1, color='k', linestyle='-', label='y1 data')
plt.xlim(0,2*np.pi)

plt.xticks([0, np.pi/2, np.pi, 3*np.pi/2, 2*np.pi],
                   ['$0$', r'$\frac{\pi}{2}$', r'$\pi$', r'$\frac{3\pi}{2}$', r'$2\pi$'],size=18)

plt.legend(loc=0)

plt.savefig('sine.eps', format='eps', dpi=600)
plt.savefig('sine.pdf', format='pdf', dpi=600)
