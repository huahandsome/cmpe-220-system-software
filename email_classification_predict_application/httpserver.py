#!/usr/bin/env python

from http.server import BaseHTTPRequestHandler, HTTPServer
import os
import pickle
import predict
import save_to_mongodb

#init mongodb
print("init mongodb")
db = save_to_mongodb.get_db()

count = 0
#Create custom HTTPRequestHandler class
class KodeFunHTTPRequestHandler(BaseHTTPRequestHandler):

    def do_POST(self):
        try:
            self.data_string = self.rfile.read(int(self.headers['Content-Length']))
            #print(self.data_string)
            restore_data = pickle.loads(self.data_string)
            #print(restore_data)
            #print(restore_data.shape)
            global count
            count += 1

            #print(count)
            predict_ret = predict.prediction(restore_data, count)
            print("#%d prediction result:%s" % (count, predict_ret))

            #save to database
            save_to_mongodb.insert_email_to_db(db, restore_data.tolist(), predict_ret)

            #send code 200 response
            self.send_response(200)

            #send header first
            self.send_header('Content-type','text-html')
            self.end_headers()
            self.wfile.write(pickle.dumps(predict_ret))

            return

        except IOError:
            self.send_error(404, 'file not found')

########################
###      METHODS     ###
########################

def run():
    print('http server is starting...')

    #ip and port of servr
    #by default http server port is 80
    server_address = ('127.0.0.1', 80)
    httpd = HTTPServer(server_address, KodeFunHTTPRequestHandler)
    print('http server is running...')
    httpd.serve_forever()

if __name__ == '__main__':
    run()
