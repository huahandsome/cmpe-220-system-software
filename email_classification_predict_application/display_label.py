import numpy as np

########################
###      METHODS     ###
########################

#method for converting tensor label to string label
def labelToString(label):
    if np.argmax(label) == 0:
        return "ham"
    else:
        return "spam"

