#!/usr/bin/env python

import http.client
import sys
import time
import pickle
import numpy as np
import import_data
import matplotlib.pyplot as plt
import psutil
import display_label
import tensorflow as tf
import roc2img

#get http server ip
http_server = sys.argv[1]

#create a connection
conn = http.client.HTTPConnection(http_server)

#headers
headers = {"Content-type": "application/x-www-form-urlencoded","Accept": "text/plain"}

test_data, label = import_data.import_test_data()

time =  []
cpu_usage = []
mem_usage = []
accuracy_rate = []

tf_mem_usage = tf.placeholder("float", None)
tf_mem_usage_summary = tf.summary.scalar("m_usage", tf_mem_usage)

tf_cpu_usage = tf.placeholder("float", None)
tf_cpu_usage_summary = tf.summary.scalar("c_usage", tf_cpu_usage)

init_op = tf.global_variables_initializer()

sess = tf.Session()
sess.run(init_op)
predict_writer = tf.summary.FileWriter('./receiver_visualization', sess.graph);

total_email = 0
correct_predict = 0

while 1:
    result_summay = []
    for i in range(len(test_data)):

        global total_email
        total_email += 1

        #tensorboard: generate memory usage
        ret = sess.run(tf_mem_usage_summary, feed_dict={tf_mem_usage: psutil.virtual_memory().used})
        predict_writer.add_summary(ret, i)

        #tensorboard: generate cpu usage
        ret = sess.run(tf_cpu_usage_summary, feed_dict={tf_cpu_usage: psutil.cpu_percent(interval=None)})
        predict_writer.add_summary(ret, i)

        string_data = pickle.dumps(test_data[i])
        #request command to server
        conn.request("POST", "", string_data, headers)
        #time.sleep(2)
        #get response from server
        rsp = conn.getresponse()

        #print server response and data
        print("http response status:%d, " % (rsp.status), end="")
        data_received = rsp.read()
        predict_ret =  pickle.loads(data_received)
        print("predict result:%s, " % (predict_ret), end="")
        actual_ret = display_label.labelToString(label[i])
        print("actual result:%s" % (actual_ret))

        if predict_ret == "ham":
            y = 1
        else:
            y = 2

        if predict_ret == actual_ret:
            c = 'g'
            global correct_predict
            correct_predict  += 1
        else:
            c = 'r'

        x = i+1

        #result_summay to link tuple of combine "predict_ret" & "actual_ret"
        result_summay.append((actual_ret, predict_ret))

        #print("color :%s, x: %d, y:%d" % (c, x, y))
        plt.figure(1)
        plt.scatter(float(x), float(y), color = c)

        mem_usage.append(psutil.virtual_memory().used)
        #print(mem_usage)

        time.append(float(x))
        cpu_usage.append(float(psutil.cpu_percent(interval=None)))
        #print(cpu_usage)

        accuracy_rate.append(float(correct_predict)/float(total_email))
        #print(correct_predict/total_email)
    #show scatter
    #plt.show()
    plt.figure(1)
    plt.title("predict result")
    plt.savefig('client_scatter.eps', format='eps', dpi=600)

    #show cpu usage
    plt.figure(2)
    plt.title("CPU Usage")
    plt.plot(time, cpu_usage)
    #plt.show()
    plt.savefig('client_cpu_usage.eps', format='eps', dpi=600)

    #show memory usage
    plt.figure(3)
    plt.title("Memory Usage")
    plt.plot(time, mem_usage)
    #plt.show()
    plt.savefig('client_mem_usage.eps', format='eps', dpi=600)

    plt.figure(4)
    #plt.plot(time, accuracy_rate)
    plt.scatter(time, accuracy_rate)
    plt.title("Accuracy Rate")
    plt.savefig('client_accuracy_rate.eps', format='eps', dpi=600)

    print(result_summay)
    #plot ROC
    roc2img.DepictROCCurve(result_summay, import_data.count_total_ham_in_test_data(label), import_data.count_total_spam_in_test_data(label), 'r')

    break
conn.close()
