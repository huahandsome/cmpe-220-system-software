#!/usr/bin/env python

import http.client
import sys
import logistic_regression_predict as predict
import time

#get http server ip
http_server = sys.argv[1]
#create a connection
conn = http.client.HTTPConnection(http_server)

#import data
_, _, testX, testY = predict.import_data()

while 1:
    cmd = input('input command (ex. GET index.html): ')
    cmd = cmd.split()

    if cmd[0] == 'exit': #tipe exit to end it
        break

    for i in range(len(testX)):
        #request command to server
        conn.request(cmd[0], testX[i])
        time.sleep(5)
"""    
    #request command to server
    conn.request(cmd[0], cmd[1])

    #get response from server
    rsp = conn.getresponse()
    
    #print server response and data
    print((rsp.status, rsp.reason))
    data_received = rsp.read()
    print(data_received)
"""
conn.close()
