import numpy as np
import os
import tarfile

########################
###      METHODS     ###
########################

def csv_to_numpy_array(filePath, delimiter):
    return np.genfromtxt(filePath, delimiter=delimiter, dtype=None)

def import_test_data():
    if "data" not in os.listdir(os.getcwd()):
        # Untar directory of data if we haven't already
        tarObject = tarfile.open("data.tar.gz")
        tarObject.extractall()
        tarObject.close()
        print("Extracted tar to current directory")
    else:
        # we've already extracted the files
        pass

    print("loading test data")
    testX = csv_to_numpy_array("data/testX.csv", delimiter="\t")
    testY = csv_to_numpy_array("data/testY.csv", delimiter="\t")
    return testX,testY

def import_training_data():
    if "data" not in os.listdir(os.getcwd()):
        # Untar directory of data if we haven't already
        tarObject = tarfile.open("data.tar.gz")
        tarObject.extractall()
        tarObject.close()
        print("Extracted tar to current directory")
    else:
        # we've already extracted the files
        pass

    print("loading training data")
    trainX = csv_to_numpy_array("data/trainX.csv", delimiter="\t")
    trainY = csv_to_numpy_array("data/trainY.csv", delimiter="\t")
    return trainX,trainY

def import_data():
    testX, testY = import_test_data()
    trainX, trainY = import_training_data()
    return trainX,trainY,testX,testY

def count_total_ham_in_test_data(test_label):
    total_ham = 0;
    for i in range(len(test_label)):
        if test_label[i][1] == 1.0:
            total_ham += 1

    return total_ham

def count_total_spam_in_test_data(test_label):
    """
    #another way to do this:
    return len(test_label) - count_total_ham_in_test_data(test_label)
    """

    total_spam = 0;
    for i in range(len(test_label)):
        if test_label[i][1] == 1.0:
            total_spam += 1

    return total_spam

if __name__ == "__main__":
    print("...test data ...")
    import_test_data()
    print("...training data ...")
    import_training_data()
    print("...data...")
    import_data()

